
// Változók beállítása
var movePaddle = 0;
var moveBullet = 0;
var paddlePos = 450;
var bullettX = paddlePos + 40;
var bullettPosY = 30;
var bombPosX, 
bombPosY, 
enemyPosX, 
bomb2PosX, 
bomb2PosY, 
enemy2PosX,
life;
var score = 0;
var giftX;
var giftY = 0;
var game = true;
var paused = false;
var sound = true;

// configuring DOM
var mainLayout = document.querySelector("#mainMenu");
var gameArea = document.querySelector("#universe");
var metroid = document.querySelector("#enemy_1");
var beholder = document.querySelector("#enemy_2");
var metroidBomb = document.querySelector("#bomb_1");
var beholderBomb = document.querySelector("#bomb_2");
var fire = document.querySelector("#bullet");
var scoreContainer = document.querySelector("#score");
var endScore = document.querySelector(".score");
var lifeContainer = document.querySelector("#life");
var endGameMessage = document.querySelector(".endGameMessage");
var newGame = document.querySelector(".again");
var newMainGame = document.querySelector(".mainAgain");
var mainMenu = document.querySelector(".main_menu");
var level0 = document.querySelector(".goose");
var level1 = document.querySelector(".baby");
var level2 = document.querySelector(".disintegrate");
var level3 = document.querySelector(".minotaur");
var level0 = document.querySelector(".goose");

// Howler beállítás, hangok, zenék
var shoot = new Howl({
  src: ['./sounds/Laser_Shoot7.wav']
});

var endSound = new Howl({
  src: ['./sounds/Shut_Down1.wav']
});

var destroyShip = new Howl({
  src: ['./sounds/Explosion4.wav']
});

var destroyEnemy = new Howl({
  src: ['./sounds/Explosion12.wav']
});

var gameMusic = new Howl({
  src: ['./music/Arcade Funk.mp3'],
  loop: true
});

var endMusic = new Howl({
  src: ['./music/Superboy.mp3'],
  loop: true
});
var mainMusic = new Howl({
  src: ['./music/Defense Line.mp3'],
  loop: true,
  onpause: function() {
  },
});

// Főmenü beállítása
function startMainMenu() {
  gameMusic.stop();
  endMusic.stop();
  mainMusic.play();
  endGameMessage.style.display = "none";
  gameArea.style.display = "none";
  mainLayout.style.display = "block";
}

// Játék alapbeállítása, ellenség véletlenszerű pozicionálása, ugyanonnan indul az első bomba is
function startNewGame() {
  game = true;
  mainLayout.style.display = "none";
  endGameMessage.style.display = "none";
  level0.style.display = "none";
  level1.style.display = "none";
  level2.style.display = "none";
  level3.style.display = "none";
  gameArea.style.display = "block";
  mainMusic.stop();
  endMusic.stop();
  gameMusic.play();
  life = 3;
  score = 0;
  
  enemyPosX = Math.floor(Math.random() * 920);
  bombPosX = enemyPosX;
  bombPosY = 30;

  enemy2PosX = Math.floor(Math.random() * 920);
  bomb2PosX = enemyPosX;
  bomb2PosY = 30;
}

// Játék befejezésének kezelése
function endGame() {
  mainMusic.stop();
  gameMusic.stop();
  endMusic.play();
  gameArea.style.display = "none";
  mainLayout.style.display = "none";
  endGameMessage.style.display = "block";
  endScore.textContent = score;
  if (score < 100) {
    level0.style.display = "block";
  }
  if (score > 100 && score < 2000) {
    level1.style.display = "block";
  }
  if (score > 2000 && score < 8000) {
    level2.style.display = "block";
  }
  if (score > 8000) {
    level3.style.display = "block";
  }
}

// Egy élet elvesztésének  ill. az összes élet elvesztésének lekezelése
function endLife() {
  life -= 1;
  if (life <= 0) {
    endSound.play();
    game = false;
    lifeContainer.textContent = " 0";
    life = 0;
    endGame();
  }
}

mainMenu.addEventListener("click", function () {
  startMainMenu();
});

newMainGame.addEventListener("click", function () {
  startNewGame();
});

newGame.addEventListener("click", function () {
  startNewGame();
});



// billentyűlenyomás figyelése, lenyomásra / "-" "+" / érték adása változónak
document.addEventListener("keydown", function (moving) {
  if (moving.keyCode == 37 || moving.which == 37) { // left arrow key
    movePaddle = -9;

  }
  if (moving.keyCode == 39 || moving.which == 39) { // right arrow Key
    movePaddle = 9;

  }
  if (moving.keyCode == 32 || moving.which == 32 && bullettPosY > 778) { // space key
    bullettX = paddlePos + 40;
    shoot.play();
    moveBullet = 7;
  } 

  if (moving.keyCode == 80 || moving.which == 80 && paused === false) { // p key
    paused = !paused;
  } 

}, false);


// billentyűfelengedés figyelése, felengedéskor változó érték "0", azaz a pad megáll
document.addEventListener("keyup", function (moving) {
  if (moving.keyCode == 37 || moving.which == 37) { // left arrow key
    movePaddle = 0;

  }
  if (moving.keyCode == 39 || moving.which == 39) { // right arrow Key
    movePaddle = 0;
  }
}, false);


// Ez a funkció teszi lehetővé a folyamatos képkocka/mp értéket
// A mozgás, grafikai elemek ezen belül kell legyenek!!!
window.setInterval(function show() {

  if (paused === false) {  // Ha a paused false, akkor megy a játék, ellenkező esetben pillanat álj.

    // Az "űrhajónk" / paddle mozgása a játéktéren belül
    paddlePos += movePaddle;
    document.getElementById("paddle").style.left = paddlePos + "px";

    if (paddlePos <= 0) {
      paddlePos = 0;
    } else if (paddlePos > 870) {
      paddlePos = 870;
    }

    // Az ellenséges űrhajók folyamatos jobbra-balra mozgatása
    enemyPosX += 4;
    if (enemyPosX > 920) {
      enemyPosX = 0;
    }

    enemy2PosX -= 3;
    if (enemy2PosX < 0) {
      enemy2PosX = 920;
    }

    // Az ellenséges bombák zuhanása, azok sebessége
    bomb2PosY += 8;
    bombPosY += 6;

    // A bombák és az űrhajó találkozásának definiálása
    if ((bombPosX + 50 > paddlePos && bombPosY >= 510) && (bombPosX - 100 < paddlePos) && bombPosY >= 510) {
      destroyShip.play();
      bombPosX = enemyPosX;
      bombPosY = 0;
      endLife();
    }

    if ((bomb2PosX + 50 > paddlePos && bomb2PosY >= 510) && (bomb2PosX - 100 < paddlePos) && bomb2PosY >= 510) {
      destroyShip.play();
      bomb2PosX = enemyPosX;
      bomb2PosY = 0;
      endLife();
    }

    // A bombák "esemény nélküli" lezuhanása, nem találnak el
    if (bombPosY >= 530 && game === true) {
      bombPosY = 0;
      bombPosX = enemyPosX;
      score += 1;
    }

    if (bomb2PosY >= 530 && game === true) {
      bomb2PosY = 0;
      bomb2PosX = enemy2PosX;
      score += 1;
    }

    // Ez mondja meg a lövedéknek hogy mindig ott legyen ahol az űrhajónk közepe
    if (bullettPosY == 30) {
      bullettX = paddlePos + 40;
    }

    // A lövedékünk "esemény nélküli" repülése, nem talál el ellenséges űrhajót
    if (bullettPosY > 540) {
      bullettPosY = 30;
      moveBullet = 0;
      bullettX = paddlePos + 40;
      fire.style.left = bullettX + "px";
    }

    // A kilőtt lövedék és az ellenséges űrhajók találkozásának kezelése
    if ((bullettX + 30 > enemyPosX && bullettPosY >= 520) && (bullettX - 50 < enemyPosX) && bullettPosY >= 520) {
      destroyEnemy.play(); 
      bullettPosY = 30;
      moveBullet = 0;
      score += 50;
    }

    else if ((bullettX + 30 > enemy2PosX && bullettPosY >= 520) && (bullettX - 50 < enemy2PosX) && bullettPosY >= 520) {
      destroyEnemy.play();
      bullettPosY = 30;
      moveBullet = 0;
      score -= 60;
    } else {
      // A korábban az űrhajó pozícióját felvett lövedék repülése függőlegesen felfelé
      bullettPosY += moveBullet;
      fire.style.display = "block";
      fire.style.bottom = bullettPosY + "px";
    }

    // Megjelenítés a layouton, DOM-on keresztül manipuláljuk az oldalt
    metroidBomb.style.left = bombPosX + "px";
    metroidBomb.style.top = bombPosY + "px";
    metroid.style.left = enemyPosX + "px";
    beholderBomb.style.left = bomb2PosX + "px";
    beholderBomb.style.top = bomb2PosY + "px";
    beholder.style.left = enemy2PosX + "px";
    lifeContainer.textContent = life;
    scoreContainer.textContent = score;
    fire.style.left = bullettX + "px";
  }
}, 1000 / 60);

startMainMenu();